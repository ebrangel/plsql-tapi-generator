<#assign
  conn = new_connection(
    "jdbc:oracle:thin:@//" + template_args[0],
    template_args[1], template_args[2]
  )
/>
<#assign user_ori = template_args[1]/>  
<#assign user_dest = template_args[3]/>  
<#assign user_tables_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'TABLE'
ORDER BY object_name
</#assign>
<#assign user_views_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'VIEW'
ORDER BY object_name
</#assign>
<#assign user_procedures_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'PROCEDURE'
ORDER BY object_name
</#assign>
<#assign user_functions_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'FUNCTION'
ORDER BY object_name
</#assign>
<#assign user_types_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'TYPE'
   AND object_name NOT LIKE 'SYS%'
ORDER BY object_name
</#assign>
<#assign user_packages_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'PACKAGE'
ORDER BY object_name
</#assign>
<#assign user_packages_filter_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'PACKAGE'
   AND object_name LIKE :1
ORDER BY object_name
</#assign>
<#assign functions = conn.query(user_functions_sql)/>

<#list functions as function>  
GRANT EXECUTE ON ${user_ori?upper_case}.${function.OBJECT_NAME} TO ${user_dest?upper_case};
</#list>

<#assign packages_filter = conn.query(user_packages_filter_sql, ["%SP"])/>
<#list packages_filter as package>   
GRANT EXECUTE ON ${user_ori?upper_case}.${package.OBJECT_NAME} TO ${user_dest?upper_case};
</#list>

<#assign packages_filter = conn.query(user_packages_filter_sql, ["%AP"])/>
<#list packages_filter as package>   
GRANT EXECUTE ON ${user_ori?upper_case}.${package.OBJECT_NAME} TO ${user_dest?upper_case};
</#list>

<#assign void = conn.close()/>