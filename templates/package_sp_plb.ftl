<#assign
  conn = new_connection(
    "jdbc:oracle:thin:@//" + template_args[0],
    template_args[1], template_args[2]
  )
/>
<#assign table_name = template_args[3]/>  
<#assign all_cols_sql>
SELECT LOWER(COLUMN_NAME) COLUMN_NAME, LOWER(TABLE_NAME) TABLE_NAME, DECODE(NULLABLE,'Y',' DEFAULT NULL',' ') DEF_NULL
  FROM USER_TAB_COLUMNS
 WHERE TABLE_NAME = UPPER('${table_name}')
 ORDER BY COLUMN_ID
</#assign>
<#assign pk_cols_sql>
SELECT LOWER (ucc.column_name) column_name, LOWER (ucc.table_name) table_name
  FROM user_constraints  uc
       INNER JOIN user_cons_columns ucc
           ON uc.constraint_name = ucc.constraint_name AND uc.owner = ucc.owner AND uc.table_name = ucc.table_name
 WHERE uc.table_name = UPPER('${table_name}')
   AND uc.constraint_type = 'P'
 ORDER BY ucc.position
</#assign>
<#assign not_pk_cols_sql>
SELECT lower(tc.column_name) column_name, DECODE(NULLABLE,'Y',' DEFAULT NULL',' ') DEF_NULL
  FROM user_tab_columns tc
 WHERE     tc.table_name = UPPER('${table_name}')
       AND tc.column_name NOT IN
               (SELECT cc.column_name
                  FROM user_cons_columns  cc
                       INNER JOIN user_constraints c
                           ON (    c.owner = cc.owner
                               AND c.table_name = cc.table_name
                               AND c.constraint_name = cc.constraint_name)
                 WHERE c.constraint_type = 'P' AND cc.table_name = tc.table_name)
 ORDER BY TC.COLUMN_ID				 
</#assign>
<#assign all_cols = conn.query(all_cols_sql)/>
<#assign pk_cols = conn.query(pk_cols_sql)/>
<#assign not_pk_cols = conn.query(not_pk_cols_sql)/>
create or replace PACKAGE body ${table_name}_SP
IS
  -- insert
  PROCEDURE ins(
<#list pk_cols as col>   
      ${col.COLUMN_NAME}_in IN OUT ${table_name}_TP.${col.COLUMN_NAME},
</#list>
<#list not_pk_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>)
  IS
  BEGIN
<#list pk_cols as col>   
            ${col.COLUMN_NAME}_in := FC_RANDOM_UUID;
</#list>	  
  
    ${table_name}_cp.ins(
<#list pk_cols as col>   
                                 ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in,
</#list>	
<#list not_pk_cols as col>   
                                 ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in,
</#list>
                                 handle_error_in => TRUE); 
  END;
  
  FUNCTION ins(
<#list all_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>) RETURN ROWID
  IS
  BEGIN
     return ${table_name}_cp.ins(
<#list all_cols as col>   
                ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in,
</#list>	          handle_error_in => TRUE); 
  END;

-- update
  PROCEDURE upd
    (
<#list all_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>)
  IS
    l_rows_out PLS_INTEGER;
  BEGIN
    ${table_name}_cp.upd(
<#list all_cols as col>   
    ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in,
</#list>    rows_out => l_rows_out);
  END;
  
    PROCEDURE upd
    (
	  rowid_in IN ROWID,
<#list all_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>)
  IS
      l_rows_out PLS_INTEGER;
  BEGIN
      ${table_name}_cp.upd(
        rowid_in => rowid_in,			
<#list all_cols as col>   
        ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in,
</#list>       rows_out => l_rows_out);
  END;

-- del
  PROCEDURE del(
<#list pk_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_tp.${col.COLUMN_NAME}<#sep>, </#sep>
</#list>)
  IS
  BEGIN
            ${table_name}_cp.del(
<#list pk_cols as col>   
                                 ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in<#sep>, </#sep>
</#list>);
  END;
  
  PROCEDURE del(rowid_in IN ROWID)
  IS
  BEGIN
      ${table_name}_cp.del(rowid_in => rowid_in);
  END;

END ${table_name}_SP;
${"/"}

<#assign void = conn.close()/>