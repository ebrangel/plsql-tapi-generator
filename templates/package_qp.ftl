<#assign
  conn = new_connection(
    "jdbc:oracle:thin:@//" + template_args[0],
    template_args[1], template_args[2]
  )
/>
<#assign table_name = template_args[3]/>  
<#assign user_tab_cols_sql>
SELECT LOWER(COLUMN_NAME) COLUMN_NAME, LOWER(TABLE_NAME) TABLE_NAME, DECODE(NULLABLE,'Y',' DEFAULT NULL') DEF_NULL
  FROM USER_TAB_COLUMNS
 WHERE TABLE_NAME = UPPER('${table_name}')
 ORDER BY COLUMN_ID
</#assign>
<#assign user_tab_key_cols_sql>
SELECT LOWER (ucc.column_name) column_name, LOWER (ucc.table_name) table_name
  FROM user_constraints  uc
       INNER JOIN user_cons_columns ucc
           ON uc.constraint_name = ucc.constraint_name AND uc.owner = ucc.owner AND uc.table_name = ucc.table_name
 WHERE uc.table_name = UPPER('${table_name}')
 AND uc.constraint_type = 'P'
 ORDER BY ucc.position
</#assign>
<#assign uk_indexes_sql>
SELECT DISTINCT uic.index_name
  FROM user_ind_columns  uic
       INNER JOIN user_indexes ui ON uic.table_name = ui.table_name AND uic.index_name = ui.index_name
 WHERE     uic.table_name = UPPER('${table_name}')
       AND ui.uniqueness = 'UNIQUE'
       AND uic.index_name NOT IN (SELECT index_name
                                    FROM user_constraints uc
                                   WHERE uc.table_name = uic.table_name AND uc.constraint_type = 'P')
</#assign>	
<#assign uk_col_indexes_sql>
SELECT LOWER(uic.column_name) COLUMN_NAME
  FROM user_ind_columns  uic
       INNER JOIN user_indexes ui ON uic.table_name = ui.table_name AND uic.index_name = ui.index_name
 WHERE     uic.table_name = UPPER('${table_name}')
       AND uic.index_name = :1
       AND ui.uniqueness = 'UNIQUE'
       AND uic.index_name NOT IN (SELECT index_name
                                    FROM user_constraints uc
                                   WHERE uc.table_name = uic.table_name AND uc.constraint_type IN ('P','U'))
ORDER BY uic.COLUMN_POSITION								   
</#assign>								   							   
<#assign ak_indexes_sql>
SELECT uc.constraint_name
  FROM user_constraints uc
 WHERE table_name = UPPER('${table_name}')
   AND constraint_type = 'U'
</#assign>	
<#assign ak_col_indexes_sql>
SELECT lower(ucc.column_name) COLUMN_NAME
  FROM user_cons_columns ucc
 WHERE ucc.constraint_name = :1
ORDER BY ucc.position
</#assign>								   							   
<#assign ix_indexes_sql>
SELECT DISTINCT uic.index_name
  FROM user_ind_columns  uic
       INNER JOIN user_indexes ui ON uic.table_name = ui.table_name AND uic.index_name = ui.index_name
 WHERE     uic.table_name = UPPER('${table_name}')
       AND ui.uniqueness = 'NONUNIQUE'
       AND uic.index_name NOT IN (SELECT index_name
                                    FROM user_constraints uc
                                   WHERE uc.table_name = uic.table_name AND uc.constraint_type = 'P')
</#assign>	
<#assign ix_col_indexes_sql>
SELECT LOWER(uic.column_name) COLUMN_NAME
  FROM user_ind_columns  uic
       INNER JOIN user_indexes ui ON uic.table_name = ui.table_name AND uic.index_name = ui.index_name
 WHERE     uic.table_name = UPPER('${table_name}')
       AND uic.index_name = :1
       AND ui.uniqueness = 'NONUNIQUE'
       AND uic.index_name NOT IN (SELECT index_name
                                    FROM user_constraints uc
                                   WHERE uc.table_name = uic.table_name AND uc.constraint_type = 'P')
ORDER BY uic.COLUMN_POSITION								   
</#assign>								   							   
<#assign columns = conn.query(user_tab_cols_sql)/>
<#assign key_columns = conn.query(user_tab_key_cols_sql)/>
<#assign uk_indexes = conn.query(uk_indexes_sql)/>
<#assign ak_indexes = conn.query(ak_indexes_sql)/>
<#assign ix_indexes = conn.query(ix_indexes_sql)/>
CREATE OR REPLACE PACKAGE ${table_name}_QP
IS
   -- Return a record for one row of data for a primary key
   FUNCTION onerow (
<#list key_columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}<#sep>,</#sep>
</#list>	  
      )
   RETURN ${table_name}_TP.${table_name}_rt;

   -- Return TRUE if a row exists for this primary key
   FUNCTION row_exists (
<#list key_columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}<#sep>,</#sep>
</#list>	  
      )
   RETURN BOOLEAN;
   
   -- Return all rows in a collection of records
   FUNCTION allrows RETURN ${table_name}_TP.${table_name}_tc;   
   
   -- Return all rows in table via ref cursor
   FUNCTION allrows_CV RETURN ${table_name}_TP.${table_name}_rc;
   
<#list uk_indexes as col>   
<#assign uk_col_indexes = conn.query(uk_col_indexes_sql,[col.INDEX_NAME])/>
<#if uk_col_indexes?size gt 0>
 -- Return a record for one row by ${col.INDEX_NAME} value
   FUNCTION or_${col.INDEX_NAME} (
<#list uk_col_indexes as col2>   
      ${col2.COLUMN_NAME}_in IN ${table_name}_TP.${col2.COLUMN_NAME}<#sep>,</#sep>
</#list>	     	  
      )
      RETURN ${table_name}_TP.${table_name}_rt;
</#if>     	  
</#list>
<#list ak_indexes as col>   
<#assign ak_col_indexes = conn.query(ak_col_indexes_sql,[col.CONSTRAINT_NAME])/>
 -- Return a record for one row by ${col.CONSTRAINT_NAME} value
   FUNCTION or_${col.CONSTRAINT_NAME} (
<#list ak_col_indexes as col2>   
      ${col2.COLUMN_NAME}_in IN ${table_name}_TP.${col2.COLUMN_NAME}<#sep>,</#sep>
</#list>	     	  
      )
      RETURN ${table_name}_TP.${table_name}_rt;
	  
</#list>	        
<#list ix_indexes as col>   
<#assign ix_col_indexes = conn.query(ix_col_indexes_sql,[col.INDEX_NAME])/>
 -- Return a record for one row by ${col.INDEX_NAME} value
   FUNCTION ar_${col.INDEX_NAME} (
<#list ix_col_indexes as col2>   
      ${col2.COLUMN_NAME}_in IN ${table_name}_TP.${col2.COLUMN_NAME}<#sep>,</#sep>
</#list>	     	  
      )
      RETURN ${table_name}_TP.${table_name}_rc;
	  
</#list>	        

END ${table_name}_QP;
${"/"}   

CREATE OR REPLACE PACKAGE BODY ${table_name}_QP
IS

   FUNCTION onerow (
<#list key_columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}<#sep>,</#sep>
</#list>	  
      )
   RETURN ${table_name}_TP.${table_name}_rt
   IS
      onerow_rec ${table_name}_TP.${table_name}_rt;
   BEGIN
      SELECT *
        INTO onerow_rec
        FROM ${table_name}
       WHERE <#list key_columns as col>${col.COLUMN_NAME} = ${col.COLUMN_NAME}_in
	     <#sep>AND </#sep></#list>;

      RETURN onerow_rec;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END onerow;   

   FUNCTION row_exists (
<#list key_columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}<#sep>,</#sep>
</#list>	  
      )
   RETURN BOOLEAN
   IS
     l_dummy PLS_INTEGER;
   BEGIN
      SELECT 1 INTO l_dummy
        FROM ${table_name}
       WHERE <#list key_columns as col>${col.COLUMN_NAME} = ${col.COLUMN_NAME}_in
	     <#sep>AND </#sep></#list>;

      RETURN TRUE;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN RETURN FALSE;
      WHEN TOO_MANY_ROWS THEN RETURN TRUE;
   END row_exists;   
   
   FUNCTION allrows RETURN ${table_name}_TP.${table_name}_tc
   IS
      CURSOR allrows_cur
      IS
         SELECT *
           FROM ${table_name};
	  
      retval ${table_name}_TP.${table_name}_tc;
   BEGIN
      OPEN allrows_cur;
      FETCH allrows_cur BULK COLLECT INTO retval;
	  
      RETURN retval;
   END allrows;   
   
   FUNCTION allrows_cv RETURN ${table_name}_TP.${table_name}_rc
   IS
      retval ${table_name}_TP.${table_name}_rc;
   BEGIN
      OPEN retval FOR
         SELECT *
           FROM ${table_name};
		   
      RETURN retval;
   END allrows_cv;   
   
<#list uk_indexes as col>   
<#assign uk_col_indexes = conn.query(uk_col_indexes_sql,[col.INDEX_NAME])/>
<#if uk_col_indexes?size gt 0>
   FUNCTION or_${col.INDEX_NAME} (
<#list uk_col_indexes as col2>   
      ${col2.COLUMN_NAME}_in IN ${table_name}_TP.${col2.COLUMN_NAME}<#sep>,</#sep>
</#list>	     	  
      )
      RETURN ${table_name}_TP.${table_name}_rt
 IS
      retval ${table_name}_TP.${table_name}_rt;
   BEGIN
      SELECT *
        INTO retval
        FROM ${table_name}
       WHERE <#list uk_col_indexes as col2>${col2.COLUMN_NAME} = ${col2.COLUMN_NAME}_in
	     <#sep>AND </#sep></#list>;

      RETURN retval;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END or_${col.INDEX_NAME};	  
</#if>     	  
</#list>   
<#list ak_indexes as col>   
<#assign ak_col_indexes = conn.query(ak_col_indexes_sql,[col.CONSTRAINT_NAME])/>

   FUNCTION or_${col.CONSTRAINT_NAME} (
<#list ak_col_indexes as col2>   
      ${col2.COLUMN_NAME}_in IN ${table_name}_TP.${col2.COLUMN_NAME}<#sep>,</#sep>
</#list>	     	  
      )
      RETURN ${table_name}_TP.${table_name}_rt
 IS
      retval ${table_name}_TP.${table_name}_rt;
   BEGIN
      SELECT *
        INTO retval
        FROM ${table_name}
       WHERE <#list ak_col_indexes as col2>${col2.COLUMN_NAME} = ${col2.COLUMN_NAME}_in
	     <#sep>AND </#sep></#list>;

      RETURN retval;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END or_${col.CONSTRAINT_NAME};	  
	  
</#list>   
<#list ix_indexes as col>   
<#assign ix_col_indexes = conn.query(ix_col_indexes_sql,[col.INDEX_NAME])/>

   FUNCTION ar_${col.INDEX_NAME} (
<#list ix_col_indexes as col2>   
      ${col2.COLUMN_NAME}_in IN ${table_name}_TP.${col2.COLUMN_NAME}<#sep>,</#sep>
</#list>	     	  
   )
      RETURN ${table_name}_TP.${table_name}_rc 
 IS
      retval ${table_name}_TP.${table_name}_rc;
   BEGIN
      OPEN retval FOR   
      SELECT *
        FROM ${table_name}
       WHERE <#list ix_col_indexes as col2>${col2.COLUMN_NAME} = ${col2.COLUMN_NAME}_in
         <#sep>AND </#sep></#list>;

      RETURN retval;
   END ar_${col.INDEX_NAME};	  
	  
</#list>   
END ${table_name}_QP;
${"/"}
<#assign void = conn.close()/>