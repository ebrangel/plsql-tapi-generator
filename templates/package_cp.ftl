<#assign
  conn = new_connection(
    "jdbc:oracle:thin:@//" + template_args[0],
    template_args[1], template_args[2]
  )
/>
<#assign table_name = template_args[3]/>  
<#assign user_tab_cols_sql>
SELECT LOWER(COLUMN_NAME) COLUMN_NAME, LOWER(TABLE_NAME) TABLE_NAME, DECODE(NULLABLE,'Y',' DEFAULT NULL',' ') DEF_NULL
  FROM USER_TAB_COLUMNS
 WHERE TABLE_NAME = UPPER('${table_name}')
 ORDER BY COLUMN_ID
</#assign>
<#assign user_tab_key_cols_sql>
SELECT LOWER (ucc.column_name) column_name, LOWER (ucc.table_name) table_name
  FROM user_constraints  uc
       INNER JOIN user_cons_columns ucc
           ON uc.constraint_name = ucc.constraint_name AND uc.owner = ucc.owner AND uc.table_name = ucc.table_name
 WHERE uc.table_name = UPPER('${table_name}')
   AND uc.constraint_type = 'P'
 ORDER BY ucc.position
</#assign>
<#assign user_tab_not_key_cols_sql>
SELECT lower(tc.column_name) column_name
  FROM user_tab_columns tc
 WHERE     tc.table_name = UPPER('${table_name}')
       AND tc.column_name NOT IN
               (SELECT cc.column_name
                  FROM user_cons_columns  cc
                       INNER JOIN user_constraints c
                           ON (    c.owner = cc.owner
                               AND c.table_name = cc.table_name
                               AND c.constraint_name = cc.constraint_name)
                 WHERE c.constraint_type = 'P' AND cc.table_name = tc.table_name)
 ORDER BY TC.COLUMN_ID				 
</#assign>
<#assign columns = conn.query(user_tab_cols_sql)/>
<#assign key_columns = conn.query(user_tab_key_cols_sql)/>
<#assign not_key_columns = conn.query(user_tab_not_key_cols_sql)/>
CREATE OR REPLACE PACKAGE ${table_name}_CP
IS

  FUNCTION get_flag_fields(value_in IN NUMBER DEFAULT 0) RETURN ${table_name}_tp.fields_ft;

  FUNCTION new_rt(
<#list columns as col>   
    ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>,</#sep>
</#list>) RETURN ${table_name}_tp.${table_name}_rt;

  -- insert
  PROCEDURE ins(
<#list columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL},
</#list>
      handle_error_in          IN BOOLEAN := TRUE );
	  
  FUNCTION ins(
<#list columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL},
</#list>
      handle_error_in          IN BOOLEAN := TRUE ) RETURN ROWID;	  

  -- update
  PROCEDURE upd(
<#list columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL},
</#list>
      rows_out OUT PLS_INTEGER,
      ignore_if_null_in IN BOOLEAN := FALSE,
      handle_error_in   IN BOOLEAN := TRUE);
	  
  PROCEDURE upd(
      rowid_in IN ROWID,
<#list columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL},
</#list>
      rows_out OUT PLS_INTEGER,
      ignore_if_null_in IN BOOLEAN := FALSE,
      handle_error_in   IN BOOLEAN := TRUE);	  
	  
  PROCEDURE upd(
<#list key_columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME},
</#list>
<#list not_key_columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME} DEFAULT NULL,
</#list>
      flag_fields_in  IN  ${table_name}_tp.fields_ft,
      rows_out        OUT PLS_INTEGER,
      handle_error_in IN  BOOLEAN := TRUE);	  
	  
  PROCEDURE upd(
      rec_in IN ${table_name}_TP.${table_name}_rt,
      rows_out OUT PLS_INTEGER,
      ignore_if_null_in IN BOOLEAN := FALSE,
      handle_error_in   IN BOOLEAN := TRUE);
	  
  PROCEDURE upd_ins
    (
      rec_in IN ${table_name}_TP.${table_name}_rt,
      rows_out OUT PLS_INTEGER,
      ignore_if_null_in IN BOOLEAN := FALSE,
      handle_error_in   IN BOOLEAN := TRUE
    );

  -- delete
  PROCEDURE del(
<#list key_columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_tp.${col.COLUMN_NAME},
</#list>  
      handle_error_in          IN BOOLEAN := TRUE);
	  
  PROCEDURE del(
      rowid_in IN ROWID,
      handle_error_in          IN BOOLEAN := TRUE);

END ${table_name}_CP;
${"/"}

create or replace PACKAGE body ${table_name}_CP
IS

  FUNCTION get_flag_fields(value_in IN NUMBER DEFAULT 0) RETURN ${table_name}_tp.fields_ft
  IS
     l_flags ${table_name}_tp.fields_ft;
  BEGIN
<#list columns as col>   
     l_flags.${col.COLUMN_NAME} := value_in;
</#list>  

     return l_flags;
  END;

  FUNCTION new_rt(
<#list columns as col>   
    ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>,</#sep>
</#list>) RETURN ${table_name}_tp.${table_name}_rt
  IS
     l_return   ${table_name}_tp.${table_name}_rt;
  BEGIN
<#list columns as col>   
     l_return.${col.COLUMN_NAME} := ${col.COLUMN_NAME}_in;
</#list>  
  
     RETURN l_return;
  END;

  -- insert
  PROCEDURE ins(
<#list columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL},
</#list>
      handle_error_in          IN BOOLEAN := TRUE )
  IS
  BEGIN
    INSERT
    INTO ${table_name}
      (
<#list columns as col>   
        ${col.COLUMN_NAME}<#sep>,</#sep>
</#list>
      )
      VALUES
      (
<#list columns as col>   
      ${col.COLUMN_NAME}_in<#sep>,</#sep>
</#list>
      );
  END;
  
  FUNCTION ins(
<#list columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL},
</#list>
      handle_error_in          IN BOOLEAN := TRUE ) RETURN ROWID
  IS
    l_rowid ROWID;
  BEGIN
    INSERT
    INTO ${table_name}
      (
<#list columns as col>   
        ${col.COLUMN_NAME}<#sep>,</#sep>
</#list>
      )
      VALUES
      (
<#list columns as col>   
      ${col.COLUMN_NAME}_in<#sep>,</#sep>
</#list>
      ) returning rowid into l_rowid;
	  
	  return l_rowid;
  END;  

-- update
  PROCEDURE upd
    (
<#list columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL},
</#list>	
      rows_out OUT PLS_INTEGER,
      ignore_if_null_in IN BOOLEAN := FALSE,
      handle_error_in   IN BOOLEAN := TRUE
    )
  IS
  BEGIN
    IF NVL (ignore_if_null_in, FALSE) THEN  
      UPDATE ${table_name}
      SET 
<#list not_key_columns as col>   
          ${col.COLUMN_NAME} = NVL(${col.COLUMN_NAME}_in, ${col.COLUMN_NAME})<#sep>,</#sep>
</#list>
      WHERE <#list key_columns as col>${col.COLUMN_NAME} = ${col.COLUMN_NAME}_in
	    <#sep>AND </#sep></#list>;  	
    ELSE
      UPDATE ${table_name}
      SET 
<#list not_key_columns as col>   
          ${col.COLUMN_NAME} = ${col.COLUMN_NAME}_in<#sep>,</#sep>
</#list>
      WHERE <#list key_columns as col>${col.COLUMN_NAME} = ${col.COLUMN_NAME}_in
	    <#sep>AND </#sep></#list>;  	
    END IF;	
	
	rows_out := sql%rowcount;
  END;
  
  PROCEDURE upd
    (
	  rowid_in IN ROWID,
<#list columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL},
</#list>	
      rows_out OUT PLS_INTEGER,
      ignore_if_null_in IN BOOLEAN := FALSE,
      handle_error_in   IN BOOLEAN := TRUE
    )
  IS
  BEGIN
    IF NVL (ignore_if_null_in, FALSE) THEN  
      UPDATE ${table_name}
      SET 
<#list columns as col>   
          ${col.COLUMN_NAME} = NVL(${col.COLUMN_NAME}_in, ${col.COLUMN_NAME})<#sep>,</#sep>
</#list>
      WHERE ROWID = rowid_in;  	
    ELSE
      UPDATE ${table_name}
      SET 
<#list columns as col>   
          ${col.COLUMN_NAME} = ${col.COLUMN_NAME}_in<#sep>,</#sep>
</#list>
      WHERE ROWID = rowid_in;  	
    END IF;	
	
	rows_out := sql%rowcount;
  END;
  
  PROCEDURE upd(
<#list key_columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME},
</#list>
<#list not_key_columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME} DEFAULT NULL,
</#list>
      flag_fields_in  IN  ${table_name}_tp.fields_ft,
      rows_out        OUT PLS_INTEGER,
      handle_error_in IN  BOOLEAN := TRUE)
  IS
  BEGIN
      UPDATE ${table_name}
      SET 
<#list not_key_columns as col>   
          ${col.COLUMN_NAME} = DECODE(flag_fields_in.${col.COLUMN_NAME}, 1, ${col.COLUMN_NAME}_in, ${col.COLUMN_NAME})<#sep>,</#sep>
</#list>
      WHERE <#list key_columns as col>${col.COLUMN_NAME} = ${col.COLUMN_NAME}_in
	    <#sep>AND </#sep></#list>;  	  
  
      rows_out := sql%rowcount;  
  END;

  PROCEDURE upd
    (
      rec_in IN ${table_name}_TP.${table_name}_rt,
      rows_out OUT PLS_INTEGER,
      ignore_if_null_in IN BOOLEAN := FALSE,
      handle_error_in   IN BOOLEAN := TRUE
    )
  IS
  BEGIN
    upd (
<#list columns as col>   
        ${col.COLUMN_NAME}_in => rec_in.${col.COLUMN_NAME},
</#list>	
        rows_out => rows_out,
        ignore_if_null_in => ignore_if_null_in,
        handle_error_in => handle_error_in
	);
  END;
  
  PROCEDURE upd_ins
    (
      rec_in IN ${table_name}_TP.${table_name}_rt,
      rows_out OUT PLS_INTEGER,
      ignore_if_null_in IN BOOLEAN := FALSE,
      handle_error_in   IN BOOLEAN := TRUE
    )
  IS
     l_rows PLS_INTEGER;
  BEGIN
    upd (
<#list columns as col>   
        ${col.COLUMN_NAME}_in => rec_in.${col.COLUMN_NAME},
</#list>	
        rows_out => l_rows,
        ignore_if_null_in => ignore_if_null_in,
        handle_error_in => handle_error_in
	);
	
	IF l_rows = 0 THEN
	  ins(
<#list columns as col>   
          ${col.COLUMN_NAME}_in => rec_in.${col.COLUMN_NAME},
</#list>	
          handle_error_in => handle_error_in
	);
	
	  l_rows := 1;
	END IF;
	
	rows_out := l_rows;	
  END;  
  
-- del
  PROCEDURE del(
<#list key_columns as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_tp.${col.COLUMN_NAME},
</#list>  
      handle_error_in          IN BOOLEAN := TRUE)
  IS
  BEGIN
    DELETE
    FROM ${table_name}
    WHERE <#list key_columns as col>${col.COLUMN_NAME} = ${col.COLUMN_NAME}_in
	  <#sep>AND </#sep></#list>;
  END;
  
  PROCEDURE del(
      rowid_in IN ROWID,
      handle_error_in          IN BOOLEAN := TRUE)
  IS
  BEGIN
    DELETE
    FROM ${table_name}
    WHERE ROWID = rowid_in;
  END;

END ${table_name}_CP;
${"/"}
<#assign void = conn.close()/>