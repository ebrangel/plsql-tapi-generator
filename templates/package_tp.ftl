<#assign
  conn = new_connection(
    "jdbc:oracle:thin:@//" + template_args[0],
    template_args[1], template_args[2]
  )
/>
<#assign table_name = template_args[3]/>  
<#assign user_tab_cols_sql>
SELECT LOWER(COLUMN_NAME) COLUMN_NAME, LOWER(TABLE_NAME) TABLE_NAME
  FROM SYS.USER_TAB_COLUMNS
 WHERE TABLE_NAME = UPPER('${table_name}')
 ORDER BY COLUMN_ID
</#assign>
<#assign columns = conn.query(user_tab_cols_sql)/>
CREATE OR REPLACE PACKAGE ${table_name}_TP
AS

   SUBTYPE ${table_name}_rt IS ${table_name}%ROWTYPE;
   SUBTYPE rowtype IS ${table_name}%ROWTYPE;
   
<#list columns as col>   
   SUBTYPE ${col.COLUMN_NAME} IS ${col.TABLE_NAME}.${col.COLUMN_NAME}%TYPE;
</#list>

    -- Ref cursors returning a row from HISTORICO_RA
    -- and a weak REF CURSOR type to use with dynamic SQL.
   TYPE ${table_name}_rc IS REF CURSOR RETURN ${table_name}%ROWTYPE;
   TYPE table_refcur IS REF CURSOR RETURN ${table_name}%ROWTYPE;
   TYPE weak_refcur IS REF CURSOR;

   -- Collection of %ROWTYPE records based on "${table_name}"
   TYPE ${table_name}_tc IS TABLE OF ${table_name}%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE ${table_name}_ntt IS TABLE OF ${table_name}%ROWTYPE;
   TYPE ${table_name}_vat IS VARRAY(100) OF ${table_name}%ROWTYPE;

   -- Same type structure, with a static name.
   TYPE aat IS TABLE OF ${table_name}%ROWTYPE INDEX BY BINARY_INTEGER;
   TYPE ntt IS TABLE OF ${table_name}%ROWTYPE;
   TYPE vat IS VARRAY(100) OF ${table_name}%ROWTYPE;
   --

<#list columns as col>   
   -- Column Collection based on column "${col.COLUMN_NAME}"   
   TYPE ${col.COLUMN_NAME}_cc IS TABLE OF ${col.TABLE_NAME}.${col.COLUMN_NAME}%TYPE INDEX BY BINARY_INTEGER;
</#list>

   TYPE fields_ft IS RECORD (
<#list columns as col>   
      ${col.COLUMN_NAME} NUMBER(1)<#sep>,</#sep>
</#list>
   );

END ${table_name}_TP;
${"/"}

<#assign void = conn.close()/>