<#assign
  conn = new_connection(
    "jdbc:oracle:thin:@//" + template_args[0],
    template_args[1], template_args[2]
  )
/>
<#assign user_tables_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'TABLE'
ORDER BY object_name
</#assign>
<#assign user_views_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'VIEW'
ORDER BY object_name
</#assign>
<#assign user_procedures_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'PROCEDURE'
ORDER BY object_name
</#assign>
<#assign user_functions_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'FUNCTION'
ORDER BY object_name
</#assign>
<#assign user_types_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'TYPE'
   AND object_name NOT LIKE 'SYS%'
ORDER BY object_name
</#assign>
<#assign user_packages_filter_sql>
SELECT object_name 
  FROM user_objects
 WHERE object_type = 'PACKAGE'
   AND object_name LIKE :1
ORDER BY object_name
</#assign>
<#assign tables = conn.query(user_tables_sql)/>
<#assign views = conn.query(user_views_sql)/>
<#assign procedures = conn.query(user_procedures_sql)/>
<#assign functions = conn.query(user_functions_sql)/>
<#assign types = conn.query(user_types_sql)/>
<#list procedures as procedure>   
@data/procedures/${procedure.OBJECT_NAME?lower_case}.sql
</#list>

<#list functions as function>   
@data/functions/${function.OBJECT_NAME?lower_case}.sql
</#list>

<#list views as view>   
@data/views/${view.OBJECT_NAME?lower_case}.sql
</#list>

<#list types as type>   
@data/types/${type.OBJECT_NAME?lower_case}.pls
</#list>

<#assign packages_filter = conn.query(user_packages_filter_sql, ["%TP"])/>
<#list packages_filter as package>   
@data/packages_gen/${package.OBJECT_NAME?lower_case}.sql
</#list>

<#assign packages_filter = conn.query(user_packages_filter_sql, ["%CP"])/>
<#list packages_filter as package>   
@data/packages_gen/${package.OBJECT_NAME?lower_case}.sql
</#list>

<#assign packages_filter = conn.query(user_packages_filter_sql, ["%QP"])/>
<#list packages_filter as package>   
@data/packages_gen/${package.OBJECT_NAME?lower_case}.sql
</#list>

<#assign packages_filter = conn.query(user_packages_filter_sql, ["%CO"])/>
<#list packages_filter as package>   
@data/packages/${package.OBJECT_NAME?lower_case}.pls
</#list>

<#assign packages_filter = conn.query(user_packages_filter_sql, ["%XP"])/>
<#list packages_filter as package>   
@data/packages/${package.OBJECT_NAME?lower_case}.pls
@data/packages/${package.OBJECT_NAME?lower_case}.plb
</#list>

<#assign void = conn.close()/>