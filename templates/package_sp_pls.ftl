<#assign
  conn = new_connection(
    "jdbc:oracle:thin:@//" + template_args[0],
    template_args[1], template_args[2]
  )
/>
<#assign table_name = template_args[3]/>  
<#assign all_cols_sql>
SELECT LOWER(COLUMN_NAME) COLUMN_NAME, LOWER(TABLE_NAME) TABLE_NAME, DECODE(NULLABLE,'Y',' DEFAULT NULL',' ') DEF_NULL
  FROM USER_TAB_COLUMNS
 WHERE TABLE_NAME = UPPER('${table_name}')
 ORDER BY COLUMN_ID
</#assign>
<#assign pk_cols_sql>
SELECT LOWER (ucc.column_name) column_name, LOWER (ucc.table_name) table_name
  FROM user_constraints  uc
       INNER JOIN user_cons_columns ucc
           ON uc.constraint_name = ucc.constraint_name AND uc.owner = ucc.owner AND uc.table_name = ucc.table_name
 WHERE uc.table_name = UPPER('${table_name}')
   AND uc.constraint_type = 'P'
 ORDER BY ucc.position
</#assign>
<#assign not_pk_cols_sql>
SELECT lower(tc.column_name) column_name, DECODE(NULLABLE,'Y',' DEFAULT NULL',' ') DEF_NULL
  FROM user_tab_columns tc
 WHERE     tc.table_name = UPPER('${table_name}')
       AND tc.column_name NOT IN
               (SELECT cc.column_name
                  FROM user_cons_columns  cc
                       INNER JOIN user_constraints c
                           ON (    c.owner = cc.owner
                               AND c.table_name = cc.table_name
                               AND c.constraint_name = cc.constraint_name)
                 WHERE c.constraint_type = 'P' AND cc.table_name = tc.table_name)
 ORDER BY TC.COLUMN_ID				 
</#assign>
<#assign all_cols = conn.query(all_cols_sql)/>
<#assign pk_cols = conn.query(pk_cols_sql)/>
<#assign not_pk_cols = conn.query(not_pk_cols_sql)/>
CREATE OR REPLACE PACKAGE ${table_name}_SP
IS

  -- insert
  PROCEDURE ins(
<#list pk_cols as col>   
      ${col.COLUMN_NAME}_in IN OUT ${table_name}_TP.${col.COLUMN_NAME},
</#list>
<#list not_pk_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>);

  FUNCTION ins(
<#list all_cols as col>   
     ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>) RETURN ROWID;

  -- update
  PROCEDURE upd(
<#list all_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>);

  PROCEDURE upd(
      rowid_in IN ROWID,
<#list all_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>);

  -- delete
  PROCEDURE del(
<#list pk_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_tp.${col.COLUMN_NAME}<#sep>, </#sep>
</#list>);

  PROCEDURE del(rowid_in IN ROWID);

END ${table_name}_SP;
${"/"}

<#assign void = conn.close()/>