<#assign
  conn = new_connection(
    "jdbc:oracle:thin:@//" + template_args[0],
    template_args[1], template_args[2]
  )
/>
<#assign table_name = template_args[3]/>  
<#assign all_cols_sql>
SELECT LOWER(COLUMN_NAME) COLUMN_NAME, LOWER(TABLE_NAME) TABLE_NAME, DECODE(NULLABLE,'Y',' DEFAULT NULL',' ') DEF_NULL
  FROM USER_TAB_COLUMNS
 WHERE TABLE_NAME = UPPER('${table_name}')
 ORDER BY COLUMN_ID
</#assign>
<#assign pk_cols_sql>
SELECT LOWER (ucc.column_name) column_name, LOWER (ucc.table_name) table_name
  FROM user_constraints  uc
       INNER JOIN user_cons_columns ucc
           ON uc.constraint_name = ucc.constraint_name AND uc.owner = ucc.owner AND uc.table_name = ucc.table_name
 WHERE uc.table_name = UPPER('${table_name}')
   AND uc.constraint_type = 'P'
 ORDER BY ucc.position
</#assign>
<#assign not_pk_cols_sql>
SELECT lower(tc.column_name) column_name
  FROM user_tab_columns tc
 WHERE     tc.table_name = UPPER('${table_name}')
       AND tc.column_name NOT IN
               (SELECT cc.column_name
                  FROM user_cons_columns  cc
                       INNER JOIN user_constraints c
                           ON (    c.owner = cc.owner
                               AND c.table_name = cc.table_name
                               AND c.constraint_name = cc.constraint_name)
                 WHERE c.constraint_type = 'P' AND cc.table_name = tc.table_name)
 ORDER BY TC.COLUMN_ID				 
</#assign>
<#assign all_cols = conn.query(all_cols_sql)/>
<#assign pk_cols = conn.query(pk_cols_sql)/>
<#assign not_pk_cols = conn.query(not_pk_cols_sql)/>
CREATE OR REPLACE PACKAGE BODY ${table_name}_AP
AS

  PROCEDURE apply_ig(
<#list all_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>)
    IS
<#list pk_cols as col>   
        l_${col.COLUMN_NAME} ${table_name}_tp.${col.COLUMN_NAME};
</#list>
    BEGIN
        CASE APEX_UTIL.GET_SESSION_STATE('APEX$ROW_STATUS')
        WHEN 'C' THEN
            ${table_name}_sp.ins(
<#list pk_cols as col>   
                                 ${col.COLUMN_NAME}_in => l_${col.COLUMN_NAME},
</#list>	
<#list not_pk_cols as col>   
                                 ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in<#sep>, </#sep>
</#list>); 
         
<#list pk_cols as col>   
            APEX_UTIL.SET_SESSION_STATE('${col.COLUMN_NAME?upper_case}', l_${col.COLUMN_NAME});
</#list>        WHEN 'U' THEN
            ${table_name}_sp.upd(
<#list all_cols as col>   
                                ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in<#sep>, </#sep>
</#list>);
        WHEN 'D' THEN
            ${table_name}_sp.del(
<#list pk_cols as col>   
                                 ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in<#sep>, </#sep>
</#list>);
        END CASE;
    END apply_ig;
	
  PROCEDURE apply_ig(
       rowid_in IN ROWID,
<#list all_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>)
    IS
	  l_rowid ROWID;
    BEGIN
        CASE APEX_UTIL.GET_SESSION_STATE('APEX$ROW_STATUS')
        WHEN 'C' THEN
            l_rowid := ${table_name}_sp.ins(
<#list all_cols as col>   
              ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in<#sep>, </#sep>
</#list>); 
         
           APEX_UTIL.SET_SESSION_STATE('ROWID', l_rowid);
        WHEN 'U' THEN
            ${table_name}_sp.upd(
			  rowid_in => rowid_in,
<#list all_cols as col>   
              ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in<#sep>, </#sep>
</#list>);
        WHEN 'D' THEN
            ${table_name}_sp.del(rowid_in => rowid_in);
        END CASE;
    END apply_ig;	
	
    PROCEDURE apply_edit(
<#list all_cols as col>   
      ${col.COLUMN_NAME}_in IN ${table_name}_TP.${col.COLUMN_NAME}${col.DEF_NULL}<#sep>, </#sep>
</#list>)
    IS
<#list pk_cols as col>   
        l_${col.COLUMN_NAME} ${table_name}_tp.${col.COLUMN_NAME};
</#list>
    BEGIN
        CASE APEX_UTIL.GET_SESSION_STATE('REQUEST')
        WHEN 'CREATE' THEN
            ${table_name}_sp.ins(
<#list pk_cols as col>   
                                 ${col.COLUMN_NAME}_in => l_${col.COLUMN_NAME},
</#list>	
<#list not_pk_cols as col>   
                                 ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in<#sep>, </#sep>
</#list>); 
         
<#list pk_cols as col>   
            APEX_UTIL.SET_SESSION_STATE('P' || apex_util.get_session_state('APP_PAGE_ID') || '_${col.COLUMN_NAME?upper_case}', l_${col.COLUMN_NAME});
</#list>        WHEN 'SAVE' THEN
            ${table_name}_sp.upd(
<#list all_cols as col>   
                                ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in<#sep>, </#sep>
</#list>);
        WHEN 'DELETE' THEN
            ${table_name}_sp.del(
<#list pk_cols as col>   
                                 ${col.COLUMN_NAME}_in => ${col.COLUMN_NAME}_in<#sep>, </#sep>
</#list>);
        ELSE
		  NULL;
        END CASE;
    END apply_edit;	

END ${table_name}_AP;
${"/"}
<#assign void = conn.close()/>